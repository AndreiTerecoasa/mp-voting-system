# MobilPRO Voting System #

Created for MobilPRO mobile apps contest by Liga Studentilor Electronisti


MPVS (MobilPRO Voting System) is a system created for a better coordination of the contest. 

Tables
 - Settings
 - Users
 - Sponsors
 - Votes
 - Aplications

Current bugs 
 * save settings form not working properly

To do
 * "view users" on sponsors page
 * "delete sponsor" on sponsors page
 * "delete user" on users page
 * users/sponsors/apps statistics on home page
 * last votes on home page
 * add view/edit user detail on users page#
d