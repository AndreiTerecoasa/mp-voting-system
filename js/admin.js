$(document).ready(function() {






	$("#add_new_setting_form").submit(function(e) {
		var key_name = $("input[name='key_name']").val();
		var key_val  = $("input[name='key_value']").val();
		if(key_name != '') {
			$.ajax({
			type: "POST",
			
			data: {
					key_name: key_name,
					key_value: key_val,
					action: "add_new_setting"
			}	
			}).done(function(data) {
				var r = JSON.parse(data);
				console.log(data);
				$(".add_new_setting_form_feedback").append(r.message).delay(5000).fadeOut();;
			}).fail(function(data) {
				console.log("fail");
			});
		}
		e.preventDefault();
		return false;
	}); 


	$("#load_settings").click(function() {
		$(".settings_list").slideUp().empty();
			$.ajax({
				type : "POST",
				url : "../include/functions.php",
				data : {
					action: "get_settings_list"
				}
				}).done(function(data) {
					console.log(data);
					$(".settings_list").append(data);
				}).fail(function(data) {
					console.log(data + "HahasdfA");
				});
		$(".settings_list").slideDown();	
	});

	$("#save_settings").on("submit", function() {
		var form_data = $(this).serialize();
		console.log(form_data);
		$.ajax({
			url: '../include/functions.php',
			type: 'POST',
			data: {
					form_data: form_data,
					action: "save_settings"
			}
		})
		.done(function(data) {
			console.log("success");
			console.log(data);
			show_feedback(1, "Done");
		})
		.fail(function(data) {
			console.log("error");
		})
		.always(function(data) {
			console.log("complete");
		});
		
		return false;
	});

	$("#add_sponsor").submit(function() {
		var name = $("input[name='name']").val();
		var image = $("input[name='image']").val();

		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: {name: name, image: image, action: "add_sponsor"}
		})
		.done(function(data) {
			console.log("success");
			console.log(data);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		

		return false;
	});


	$("#user_type").change(function() {
		if($(this).val() == 'sponsori') {
			$("#sponsori").slideDown();
		}
	});

	$("#add_user").submit(function() {
		var name = $("input[name='name']").val();
		var username = $("input[name='username']").val();
		var password = $("input[name='password']").val();
		var type = $("select[name='type']").val();
		var sponsor = $("select[name='sponsor']").val();
		var title = $("input[name='title']").val();

		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: {
				action: "add_user",
				name: name,
				username: username,
				password: password,
				type: type,
				sponsor: sponsor,
				title: title
				}
		})
		.done(function(data) {
			console.log("success");
			console.log(data);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		



		return false;
	});

	$("#add_app").submit(function() {
		var name = $("input[name='app_name']").val();
		var author = $("input[name='app_author']").val();
		var photo = $("input[name='app_photo']").val();
		// var desc = $("textarea[name='app_description']").val();
		var desc = CKEDITOR.instances['app_description'].getData();
		console.log(desc);

		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: {
				name: name,
				author: author,
				photo: photo,
				desc: desc,
				action: "add_app"
			},
		})
		.done(function(data) {
			console.log(data);
			console.log("success");
			show_feedback(data['type'],data['message']);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
		return false;
	});

	$("#delete_app").click(function() {
		var id = $(this).data("app-id");
		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: {
                 action: "delete_app",
                 id: id
			}
		})
		.done(function(data) {
			var data = JSON.parse(data);
			console.log("success");
			console.log(data + " | " + data.type + " | " + data['message']);
			show_feedback(data['type'],data['message']);
			// window.location.href = "http://localhost/mpvs/admin/apps";

		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
		

		return false;
	});


	$(".load_main_app").on("click", function() {
		var id = $(this).data("app-id");
		var userid = $(this).data("user-id");
		console.log(id);

		var app_s = $(".front .app");

		app_s.slideDown();
		$("html, body").animate({
			scrollTop: app_s.offset().top,
			},
			'slow', function() {
				$(".close_app").fadeIn();
				$.ajax({
					url: ajaxurl,
					type: 'POST',
					data: {
						action: "get_app_main_view",
						id: id,
						userid: userid
					},
				})
				.done(function(data) {
					// console.log(data);
					data = JSON.parse(data);
					if(data.type == 1) {
						app_s.html(data.html);
					} else {
						console.log(data);
					}
				})
				.fail(function() {
					
				})
				.always(function() {
					
				});
			});
	});

	$(".close_app").on("click", function() {
		$(".front .app").html('');
		$(".front .app").slideUp();
		$(this).fadeOut();
	});











	//vot aplicatie
	$(document).on("submit", "#app_vote_form", function() {
		var counter = 0;
		$(".vote_field").each(function() {
			var val = $(this).val();

			var good_value = new RegExp('^[0-5]$');

			if(good_value.test(val)) {
				console.log("buna");
				$(this).css("border","1px solid #ccc");
				counter = counter + 1;

			} else {
				console.log("rea");
				$(this).css("border","1px solid red");
			}
		});

		if(counter != 5) {
			show_feedback(0, "Valorile introduse nu corespund formatului acceptat!");
			return false;
		}

		var app_id = $(this).find("input[name='app_id']").val();
		var vote_exists = $(this).find("input[name='vote_exists']").val();
		var vote_id = $(this).find("input[name='vote_id']").val();
		var vote_author = $(this).find("input[name='vote_author']").val();
		var mp_vote_c_inov = $(this).find("input[name='mp_vote_c_inov']").val();
		var mp_vote_c_des = $(this).find("input[name='mp_vote_c_des']").val();
		var mp_vote_c_expl_res = $(this).find("input[name='mp_vote_c_expl_res']").val();
		var mp_vote_c_code_q = $(this).find("input[name='mp_vote_c_code_q']").val();
		var mp_vote_c_pres = $(this).find("input[name='mp_vote_c_pres']").val();

		console.log(mp_vote_c_inov + " | " + mp_vote_c_des + " | " + mp_vote_c_expl_res + " | " + mp_vote_c_code_q + " | " + mp_vote_c_pres + " | ");

		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: {
				action:"app_vote",
				app_id: app_id,
				vote_exists: vote_exists,
				vote_id: vote_id,
				vote_author: vote_author,
				c_inov: mp_vote_c_inov,
				c_des: mp_vote_c_des,
				c_expl_res: mp_vote_c_expl_res,
				c_code_q: mp_vote_c_code_q,
				c_pres: mp_vote_c_pres
			},
		})
		.done(function(data) {
			var data = JSON.parse(data);
			$(".front .app").html('');
			$(".front .app").slideUp();
			$(".close_app").fadeOut();
			show_feedback(data.type,data.message);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		return false;
	});

	$(document).on("change", ".vote_field", function() {
		var el = $(this);
		var val = el.val();

		var good_value = new RegExp('^[0-5]$');

		if(good_value.test(val)) {
			console.log("buna");
			$(this).css("border","1px solid #ccc");
		} else {
			console.log("rea");
			$(this).css("border","1px solid red");
			return false;
		}
	});





	$(".my_votes_table_full").find(".vote_field").change(function() {
		console.log("a");
		var vote_id = $(this).parent().parent().data("mp-vote-id");
		var cr = $(this).attr("name");
		var value = $(this).val();
		
		var good_value = new RegExp('^[0-5]$');

		if(good_value.test(value)) {
			console.log("buna");
			$(this).css("border","1px solid #ccc");
		} else {
			console.log("rea");
			$(this).css("border","1px solid red");
			return false;
		}

		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: {
				action: "update_vote_single_val",
				vote_id: vote_id,
				cr: cr,
				value: value
			}
		}).done(function(data) {
			var data = JSON.parse(data);
			show_feedback(data.type, data.message);
		});
	});



});


function show_feedback(type, message) {
	$(document).ready(function() {
		var f = $("#feedback");
		if(type == 1) {
			f.css("background","#3a5df7");
			f.empty();
			f.text(message);
			f.fadeIn(1000).delay(3000).fadeOut();
		} else {
			f.css("background","#f73a57");
			f.empty();
			f.text(message);
			f.fadeIn(1000).delay(3000).fadeOut();
		}
	});
	
}