<?php require("../include/functions.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<?php mp_head(); ?>
	
	
</head>
<body class="main">
	<?php login_check(); ?>
	<div class="row no-gutter">
		<section class="left col-md-3">
			<?php include("sidebar.php"); ?>
		</section>
		<section class="right col-md-9">
			<div class="page-header">
				<h1 class="title">Add User</h1>
			</div>
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md 3">
						<form action="" name="add_user" id="add_user">
							<div class="form-group">
								<label for="">Nume, Prenume</label>
								<input type="text" name="name" class="form-control" placeholder="John Doe">
							</div>
							<div class="form-group">
								<label for="">Titlu</label>
								<input type="text" name="title" class="form-control" placeholder="John Doe">
							</div>
							<div class="form-group">
								<label for="">Username</label>
								<input type="text" name="username" class="form-control" placeholder="johndoe">
							</div>
							<div class="form-group">
								<label for="">Parola</label>
								<input type="text" name="password" class="form-control" placeholder="1324">
							</div>
							<div class="form-group">
								<label for="">Tip</label>
								<select name="type" id="user_type" class="form-control">
									<option value="">Selecteaza tip user</option>
									<option value="admin">Admin</option>
									<option value="etti">Etti</option>
									<option value="lse">LSE</option>
									<option value="sponsori">Sponsor</option>
								</select>
							</div>
							<div class="form-group" id="sponsori" style="display:none">
								<label for="">Sponsor</label>
								<select name="sponsor" id="" class="form-control">
									<option value="">Selecteaza sponsor</option>
									<?php list_sponsors_options(); ?>
								</select>
							</div>
						

							<button type="submit" class="btn btn-default" id="add_user_button">
								<span class="glyphicon glyphicon-plus"></span>
								Adauga User
							</button>
						</form>
							
							<!-- <button type="button" class="btn btn-default" id="edit_app">
								<span class="glyphicon glyphicon-plus"></span>
								Editeaza Aplicatie
							</button> -->
						</div>
					</div>
				</div>
			</div>
		<?php mp_footer(); ?>
		</section>
	</div>
</body>
</html>
<script>
	// CKEDITOR.replace('app_description');
</script>