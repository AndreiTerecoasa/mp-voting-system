<?php require("../include/functions.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<?php mp_head(); ?>
	<script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>
</head>
<body class="main">

	<?php login_check(); ?>
	<div class="row no-gutter">
		<section class="left col-md-3">
			<?php include("sidebar.php"); ?>
		</section>
		<section class="right col-md-9">
			<?php if(isset($_GET['id'])) { ?>
				<div class="page-header">
					<h1 class="title">Application: <?php app_title($_GET['id']); ?></h1>
				</div>

			<div class="row no-gutter">

				<?php get_edit_app_form($_GET['id']); ?>
				<br/>
				<p><button type="button" id="delete_app" data-app-id="<?php echo $_GET['id']; ?>" class="btn btn-danger">Sterge Aplicatie</button></p>
			</div>
			<?php } else {?> 
			<div class="page-header">
					<h1 class="title">Applications</h1>
				</div>

			<div class="row no-gutter">
				<?php list_apps(); ?>
			</div>
			<?php } ?>
			<?php mp_footer(); ?>
		</section>
	</div>
</body>
</html>