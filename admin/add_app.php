<?php require("../include/functions.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<?php mp_head(); ?>
	<script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>
</head>
<body class="main">
	<?php login_check(); ?>
	<div class="row no-gutter">
		<section class="left col-md-3">
			<?php include("sidebar.php"); ?>
		</section>
		<section class="right col-md-9">
			<div class="page-header">
				<h1 class="title">Add App</h1>
			</div>
			<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="col-md 3">
					<form action="" name="add_app" id="add_app">
						<div class="form-group">
							<label for="">Nume aplicatie</label>
							<input type="text" name="app_name" class="form-control" placeholder="Nume aplicatie">
						</div>
						<div class="form-group">
							<label for="">Autor aplicatie</label>
							<input type="text" name="app_author" class="form-control" placeholder="Autorul/Echipa">
						</div>
						<div class="form-group">
							<label for="">Poza</label>
							<input type="text" name="app_photo" class="form-control" placeholder="http://.....">
						</div>
						<div class="form-group">
							<label for="">Descriere</label>
							<textarea name="app_description" id="app_description" class="form-control" cols="30" rows="10"></textarea>
						</div>


						<button type="submit" class="btn btn-default" id="add_app_button">
							<span class="glyphicon glyphicon-plus"></span>
							Adauga Aplicatie
						</button>
					</form>
						
						<!-- <button type="button" class="btn btn-default" id="edit_app">
							<span class="glyphicon glyphicon-plus"></span>
							Editeaza Aplicatie
						</button> -->
					</div>
				</div>
			</div>
				
			</div>
		</section>
	</div>
</body>
</html>
<script>
	CKEDITOR.replace('app_description');
</script>