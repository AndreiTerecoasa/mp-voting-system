<div class="edition info">
	<h2 class="name"><?php get_setting('site-title'); ?></h2>
	<p class="type pull-right"><?php get_setting('edition-year'); ?></p>
</div>
<div class="user info">
	<h3 class="name">Andrei Terecoasa</h3>
	<p class="type">admin</p>
</div>
<div class="nav">
	<h4>Menu</h4>
	<ul>
		<li><a href="<?php echo ADMIN; ?>index"><span class="glyphicon glyphicon-home"></span>Acasa</a></li>
		<li><a href="<?php echo ADMIN; ?>users"><span class="glyphicon glyphicon-list-alt"></span>Useri</a>
			<ul>
				<li><a href="<?php echo ADMIN; ?>add_user"><span class="glyphicon glyphicon-plus"></span>Adauga User</a></li>
			</ul>
		</li>
		<li><a href="<?php echo ADMIN; ?>apps"><span class="glyphicon glyphicon-list"></span>Aplicatii</a>
			<ul>
				<li><a href="<?php echo ADMIN; ?>add_app"><span class="glyphicon glyphicon-plus"></span>Adauga Aplicatie</a></li>
			</ul>
		</li>
		<li><a href="settings"><span class="glyphicon glyphicon-list-alt"></span>Setari</a>
			<ul>
				<li><a href="<?php echo ADMIN; ?>sponsors">Sponsori</a>
					<ul>
						<li><a href="<?php echo ADMIN; ?>add_sponsor"><span class="glyphicon glyphicon-plus"></span>Adauga Sponsor</a></li>
					</ul>
				</li>
			</ul>
		</li>
	</ul>
</div>