<?php require("../include/functions.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<?php mp_head(); ?>
	<script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>
	
</head>
<body class="main">
	<?php login_check(); ?>
	<div class="row no-gutter">
		<section class="left col-md-3">
			<?php include("sidebar.php"); ?>
		</section>
		<section class="right col-md-9">
			<div class="page-header">
				<h1 class="title">Sponsori</h1>
			</div>
			<div class="row">
				<?php list_sponsors(); ?>				
			</div>
		</section>
	</div>
</body>
</html>
