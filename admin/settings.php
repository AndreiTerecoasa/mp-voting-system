<?php require("../include/functions.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<?php mp_head(); ?>
</head>
<body class="main">
	<?php login_check(); ?>
	<div class="row no-gutter">
		<section class="left col-md-3">
			<?php include("sidebar.php"); ?>
		</section>
		<section class="right col-md-9">
			<div class="page-header">
				<h1 class="title">Settings</h1>
			</div>
			<div class="panel panel-default settings-panel">
				<div class="panel-heading">Settings <button class="btn btn-info btn-xs pull-right" id="load_settings">Refresh</button></div>
				<div class="panel-body">
					<form action="" method="POST" id="save_settings" name="save_settings">
					<div class="settings_list">
						<?php echo get_settings_list(); ?>
					</div>
					
					<button type="submit" class="btn btn-primary" name="save_settings_button" id="save_settings_button">Save</button>
				</form>
				</div>
			</div>
			<div class="panel panel-default add-new-setting-panel">
				<div class="panel-heading">Add new Setting</div>
				<div class="panel-body">
					<form action="" method="POST" id="add_new_setting_form" name="add_new_setting_form">
						<div class="form-group">
							<label for="Key name">Setting name</label>
							<input type="text" class="form-control" name="key_name" placeholder="Enter key name" value="">
						</div>
						<div class="form-group">
							<label for="New Value">Setting value</label>
							<input type="text" class="form-control" name="key_value" placeholder="Enter key name" value="">
						</div>
						<button type="submit" class="btn btn-primary" name="add_new_setting">Add</button>
					</form>
					<div class="add_new_setting_form_feedback"></div>
				</div>
			</div>
		<?php mp_footer(); ?>
		</section>
	</div>
</body>
</html>