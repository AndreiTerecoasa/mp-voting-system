<?php require("../include/functions.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<?php mp_head(); ?>
	
	
</head>
<body class="main">
	<?php login_check(); ?>
	<div class="row no-gutter">
		<section class="left col-md-3">
			<?php include("sidebar.php"); ?>
		</section>
		<section class="right col-md-9">
			<div class="page-header">
				<h1 class="title">Adauga Sponsor</h1>
			</div>
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md 3">
						<form action="" method="POST" name="add_sponsor" id="add_sponsor">
							<div class="form-group">
								<label for="">Nume</label>
								<input type="text" name="name" class="form-control" placeholder="IBM">
							</div>
							<div class="form-group">
								<label for="">Imagine</label>
								<input type="text" name="image" class="form-control" placeholder="http://...">
							</div>
							<button type="submit" class="btn btn-default" name="add_sponsor_button">
								<span class="glyphicon glyphicon-plus"></span>
								Adauga Sponsor
							</button>
						</form>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</body>
</html>
