<?php
	require("include/functions.php");
	authentication();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<?php mp_head('admin/'); ?>
	<link rel="stylesheet" href="<?php echo HOME; ?>css/main.css">
  
</head>
<body class="front">
	<section id="header">
		<?php include("menu.php"); ?>
	</section>
	<section class="main row no-gutter">
		<div class="panel panel-default">
			<div class="panel-heading">Voturile Mele</div>
			<div class="panel-body row">
				<?php main_list_votes($_SESSION['mp_user_id']); ?>
			</div>
		</div>
	</section>
	<section id="footer">
		<div id="feedback"></div>
	</section>
</body>
</html>
