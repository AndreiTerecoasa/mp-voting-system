<?php

require("mpdb.php");


//some config

define('HOME', "http://localhost/mpvs/");
define('ADMIN', "http://localhost/mpvs/admin/");
	
function initial_setup() {
	global $mpdb;


	$aplications = "CREATE TABLE IF NOT EXISTS `mp_aplications` (
					`mp_app_id` INT(2) NOT NULL AUTO_INCREMENT,
					`mp_app_name` VARCHAR(300) NOT NULL,
					`mp_app_author` VARCHAR(300) NOT NULL,
					`mp_app_picture` VARCHAR(200),
					`mp_app_description` TEXT,
					PRIMARY KEY(`mp_app_id`)) 
					DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

	$users       = "CREATE TABLE IF NOT EXISTS `mp_users` (
					`mp_user_id` INT(2) NOT NULL AUTO_INCREMENT,
					`mp_user_username` VARCHAR(300) NOT NULL,
					`mp_user_password` VARCHAR(20) NOT NULL,
					`mp_user_name` VARCHAR(300),
					`mp_user_type` VARCHAR(300),
					`mp_user_team` VARCHAR(30),
					`mp_user_title` VARCHAR(300),
					PRIMARY KEY(`mp_user_id`))
					DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

	$logins      = "CREATE TABLE IF NOT EXISTS `mp_login_attempts` (
					`mp_login_id` INT(2) NOT NULL AUTO_INCREMENT,
					`mp_login_username` VARCHAR(200) NOT NULL,
					`mp_login_password` VARCHAR(200) NOT NULL,
					`mp_login_date` DATETIME,
					`mp_login_type` INT(1), /*succes = 1, fail 1*/
					PRIMARY KEY(`mp_login_id`))
					DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

	$votes       = "CREATE TABLE IF NOT EXISTS `mp_votes` (
					`mp_vote_id` INT(2) NOT NULL AUTO_INCREMENT,
					`mp_vote_app_id` INT(2) NOT NULL,
					`mp_vote_author` INT(2) NOT NULL,
					`mp_vote_c_inov` INT DEFAULT 0,
					`mp_vote_c_des` INT DEFAULT 0,
					`mp_vote_c_expl_res` INT DEFAULT 0,
					`mp_vote_c_code_q` INT DEFAULT 0,
					`mp_vote_c_pres` INT DEFAULT 0,
					`mp_vote_date` DATETIME,
					PRIMARY KEY(`mp_vote_id`))
					DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

	$settings    = "CREATE TABLE IF NOT EXISTS `mp_settings` (
					`mp_setting_id` INT(2) NOT NULL AUTO_INCREMENT,
					`mp_setting_name` VARCHAR(20) NOT NULL,
					`mp_setting_value` VARCHAR(1000),
					PRIMARY KEY(`mp_setting_id`))
					DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

	$sponsors    = "CREATE TABLE IF NOT EXISTS `mp_sponsors` (
					`mp_sponsor_id` INT(2) NOT NULL AUTO_INCREMENT,
					`mp_sponsor_name` VARCHAR(50) NOT NULL,
					`mp_sponsor_image` VARCHAR(200) NOT NULL,
					PRIMARY KEY(`mp_sponsor_id`))
					DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

	if(!$res_apps = $mpdb->query($aplications)) {
		die("There was an error running this query". $mpdb->error);
	}

	if(!$res_users= $mpdb->query($users)) {
		die("There was an error running this query". $mpdb->error);
	}

	if(!$res_votes= $mpdb->query($votes)) {
		die("There was an error running this query". $mpdb->error);
	}

	if(!$res_settings= $mpdb->query($settings)) {
		die("There was an error running this query". $mpdb->error);
	}

	if(!$res_sponsors = $mpdb->query($sponsors)) {
		die("There was an error running this query". $mpdb->error);
	}

	if(!$res_logins = $mpdb->query($logins)) {
		die("There was an error running this query". $mpdb->error);
	}

	if($res_apps && $res_users && $res_votes) {
		echo "<p>Everything is installed!</p>";
	}

}	

function get_settings_list() {
	global $mpdb;

	$settings = "SELECT * FROM `mp_settings`";

	$settings_a = $mpdb->query($settings);
	$return = '';
	while($row = $settings_a->fetch_assoc()) {
		$return .= "<div class='input-group'>
			    <span class='input-group-addon' id='basic-addon1'>{$row['mp_setting_name']}</span>
			    <input type='text' class='form-control' placeholder='Username' aria-describedby='basic-addon1' name='{$row['mp_setting_name']}' value='{$row['mp_setting_value']}'>
			  </div>";
	} 
	echo $return;
}

function add_new_setting() {
	global $mpdb;
	$key_name = $mpdb->real_escape_string($_POST['key_name']);
	$key_val  = $mpdb->real_escape_string($_POST['key_value']);
	$r        = array(
				'type' => 0,
				'message' => '');
	$add_setting = "INSERT INTO `mp_settings`(`mp_setting_name`,`mp_setting_value`)
					VALUES ('{$key_name}','{$key_val}')";

	if(($res = $mpdb->query($add_setting))) {
		$r['type'] = 1;
		$r['message'] = "<p class='bg-succes'>Done!</p>";
	}
	
	echo json_encode($r);
	die();
}

function the_setting($setting='') {
	if(setting_exists($setting)) {
		global $mpdb;
		$set = $mpdb->real_escape_string($setting);
		if($set != '') {
			$get_set = "SELECT `mp_setting_value` FROM `mp_settings` WHERE `mp_setting_name`='{$set}'";
			$rez = $mpdb->query($get_set)->fetch_assoc();
			return $rez['mp_setting_value'];
		
		} else {
			echo "No argument provided!";
		}
	}
}

function get_setting($setting='') {
	if(setting_exists($setting)) {
		global $mpdb;
		$set = $mpdb->real_escape_string($setting);
		if($set != '') {
			$get_set = "SELECT `mp_setting_value` FROM `mp_settings` WHERE `mp_setting_name`='{$set}'";
			$rez = $mpdb->query($get_set)->fetch_assoc();
			echo $rez['mp_setting_value']; 
		
		} else {
			echo "No argument provided!";
		}
	}
}

function save_settings() {
	global $mpdb;

	$data = array();
	parse_str($_POST['form_data'], $data);
	foreach($data as $key => $val) {
		$data[$key] = $mpdb->real_escape_string($val);
	}

	$query = "";
	$response = array();
	foreach($data as $key => $val) {
		if(setting_exists($key)) {
			$query .= "UPDATE `mp_settings` SET `mp_setting_value` = '$val' WHERE `mp_setting_name` = '$key';";
		}
	}

	if(($res = $mpdb->query($query))) {
		$response['type'] = 1;
		$response['message'] = "Done";
		$response['content'] = $res;
		$response['query'] = $query;
	} else {
		$response['type'] = 0;
		$response['message'] = 'false';
		$response['content'] = $res;
		$response['query'] = $query;
		$response['data'] = $data;
	}
	echo json_encode($response);
	die();

}


function setting_exists($setting= '') {
	global $mpdb;

	$query = "SELECT * FROM `mp_settings` WHERE `mp_setting_name`='{$setting}'";
	if(($res = $mpdb->query($query))) {
		if($res->num_rows == 1) {
			return true;
		} else {
			return false;
		}
	}
}

//sponsors section//



function add_sponsor() {
	global $mpdb;

	$name = $mpdb->real_escape_string($_POST['name']);
	$image = $mpdb->real_escape_string($_POST['image']);
	$response  = array();
	$insert = "INSERT INTO `mp_sponsors`(`mp_sponsor_name`,`mp_sponsor_image`) VALUES('{$name}','{$image}')";

	if(($res = $mpdb->query($insert))) {
		$response['type'] = 1;
		$response['message'] = "Added";
	} else {
		$response['type'] = 0;
		$response['message'] = $res;
	}

	echo json_encode($response);
	die();
}


function list_sponsors() {
	global $mpdb;

	$html = "";

	$query = "SELECT * FROM `mp_sponsors`";

	$res = $mpdb->query($query);

	if($res) {
		while($row = $res->fetch_assoc()) {
			$html .= "<div class='col-md-3'>";
			$html      .= "<div class='thumbnail'>";
			$html         .= "<img src='{$row['mp_sponsor_image']}' alt=''>";
			$html             .= "<div class='caption'>";
			$html                .= "<h3>{$row['mp_sponsor_name']}</h3>";
			$html                .= "<p><a href='#' class='btn btn-info btn-xs' role='button'>Vezi Useri</a> <a href='#' class='btn btn-danger btn-xs delete_sponsor' role='button' data-id='{$row['mp_sponsor_id']}'>Sterge</a></p>";
			$html             .= "</div>";
			$html      .= "</div>";
			$html .= "</div>";
		}
	}

	echo $html;
}

function list_sponsors_options() {
	global $mpdb;

	$html = "";

	$query = "SELECT * FROM `mp_sponsors`";

	$res = $mpdb->query($query);

	if($res) {
		while($row = $res->fetch_assoc()) {
			$html .= "<option value='{$row['mp_sponsor_name']}'>{$row['mp_sponsor_name']}</option>";
		}
	}

	echo $html;
}




//users section

function add_user() {
	global $mpdb;
	$resp = array();
	$name = $mpdb->real_escape_string($_POST['name']);
	$username = $mpdb->real_escape_string($_POST['username']);
	$password = $mpdb->real_escape_string($_POST['password']);
	$type = $mpdb->real_escape_string($_POST['type']);
	$sponsor = $mpdb->real_escape_string($_POST['sponsor']);
	$title = $mpdb->real_escape_string($_POST['title']);

	$insert = "INSERT INTO `mp_users`(`mp_user_name`,`mp_user_username`,`mp_user_password`,`mp_user_type`,`mp_user_team`,`mp_user_title`)
				VALUES('{$name}','{$username}','{$password}','{$type}','{$sponsor}','{$title}')";

	if(($res = $mpdb->query($insert))) {
		$resp['type'] = 1;
		$resp['message'] = "Done";
		$resp['content'] = $_POST;
	} else {
		$resp['type'] = 0;
		$resp['message'] = "Ups. Ceva a mers prost";
	}

	echo json_encode($resp);
	die();
}


function list_users() {
	global $mpdb;
	$users = "SELECT * FROM `mp_users`";

	$res = $mpdb->query($users);
	$html = "";
	$html .= "<div class='list-group'>";
	if($res) {
		while($row = $res->fetch_assoc()) {
			$html .= "<a href='#' class='list-group-item'>";
			$html .=   "<h4 class='list-group-item-heading'>{$row['mp_user_name']}</h4>";
			$html .=   "<p class='list-group-item-text'>";
			$html .=   "<span>{$row['mp_user_title']}</span> | <span>{$row['mp_user_type']}</span> | <span>{$row['mp_user_team']}</span>";
			$html .= "</p></a>";
		}
	}
	$html .= "</div>";
	echo $html;
}

function main_list_team_members($team = '', $id = '') {
	global $mpdb;

	$team = $mpdb->real_escape_string($team);

	$team = "SELECT * FROM `mp_users` WHERE `mp_user_team` = '{$team}'";

	$res = $mpdb->query($team);

	if($res) {
		$html ="";
		while($member = $res->fetch_assoc()) {
			if(strcmp($id, $member['mp_user_id']) !== 0) {
				$html .= $member['mp_user_name'] . "<br/>";
			}	
		}
	}
	echo $html;
}	



//application part

function add_app() {
	global $mpdb;
	$response = array();
	$name = $mpdb->real_escape_string($_POST['name']);
	$author = $mpdb->real_escape_string($_POST['author']);
	$photo = $mpdb->real_escape_string($_POST['photo']);
	$desc = $mpdb->real_escape_string($_POST['desc']);

	$add_app = "INSERT INTO `mp_aplications`(`mp_app_name`,`mp_app_author`,`mp_app_picture`,`mp_app_description`)
										VALUES('{$name}','{$author}','{$photo}','{$desc}')";

	$res = $mpdb->query($add_app);
	if($res) {
		$response['type'] = 1;
		$response['message'] = "Done";
	} else {
		$response['type'] = 0;
		$response['message'] = "Ups";
	}

	echo json_encode($response);
	die();

}

function list_apps() {
	global $mpdb;

	$html = "";

	$apps = "SELECT * FROM `mp_aplications`";

	$res = $mpdb->query($apps);
	$html .= "<div class='list-group'>";
	if($res) {
		while($app = $res->fetch_assoc()) {
			$html .= "<a href='http://localhost/mpvs/admin/apps/{$app['mp_app_id']}/' class='list-group-item'>";
			$html .=   "<h4 class='list-group-item-heading'>{$app['mp_app_name']}</h4>";
			$html .=   "<div class='list-group-item-text'>";
			$html .=   "<span class='glyphicon glyphicon-education'></span> {$app['mp_app_author']}";
			$html .= "</div>";
			$html .= "</a>";
		}
	}
	$html .= "</div>";
	echo $html;
}

function app_title($id ='') {
	global $mpdb;
	$id = $mpdb->real_escape_string($id);
	$app = "SELECT * FROM `mp_aplications` WHERE `mp_app_id`=$id";
	echo $mpdb->query($app)->fetch_assoc()['mp_app_name'];
}

function get_edit_app_form($id='') {
	global $mpdb;
	$id = $mpdb->real_escape_string($id);
	$app = "SELECT * FROM `mp_aplications` WHERE `mp_app_id`=$id";

	$res = $mpdb->query($app);
	$form = "";
	if($res) {
		$app = $res->fetch_assoc();
		$form .="<form action='' name='edit_app' id='edit_app'>
					<div class='form-group'>
						<label for=''>Nume aplicatie</label>
						<input type='text' name='app_name' class='form-control' value='{$app['mp_app_name']}' placeholder='Nume aplicatie'>
					</div>
					<div class='form-group'>
						<label for=''>Autor aplicatie</label>
						<input type='text' name='app_author' class='form-control' value='{$app['mp_app_author']}' placeholder='Autorul/Echipa'>
					</div>
					<div class='form-group'>
						<label for=''>Poza</label>
						<input type='text' name='app_photo' class='form-control' value='{$app['mp_app_picture']}' placeholder='http://.....'>
					</div>
					<div class='form-group'>
						<label for=''>Descriere</label>
						<textarea name='app_description' id='app_description' value='{$app['mp_app_description']}' class='form-control' cols='30' rows='10'>{$app['mp_app_description']}</textarea>
					</div>


					<button type='submit' class='btn btn-primary' id='edit_app_button'>
						Salveaza Aplicatie
					</button>
				</form><script>CKEDITOR.replace('app_description');</script>";
	}

	echo $form;
}

function delete_app() {
	global $mpdb;
	$response = array();
	$id = (int) $mpdb->real_escape_string($_POST['id']);

	$delete_app = "DELETE FROM `mp_aplications`
				   WHERE `mp_app_id`=$id";

	if(($res = $mpdb->query($delete_app))) {
		$response['type'] = 1;
		$response['message'] = "Done";
		$response["content"] = $res;
	} else {
		$response['type'] = 0;
		$response['message'] = "Ups";
		$response["content"] = $res;
	}

	echo json_encode($response);
	die();
}

function main_list_apps() {
	global $mpdb;
	$html = "";
	$query = "SELECT * FROM `mp_aplications`";

	$res = $mpdb->query($query);

	if($res) {
		while($app = $res->fetch_assoc()) {
			$html .= "<div class='col-md-4'>";
			$html   .= "<div class='thumbnail'>";
			$html     .= "<img src='{$app['mp_app_picture']}' alt=''>";
			$html     .= "<div class='caption'>";
			$html       .= "<h3>{$app['mp_app_name']} <span class='badge badge-info'>Votat</span></h3>";
			$html       .= "<p>Autor: {$app['mp_app_author']}</p>";
			$html       .= "<p><button type='button' data-app-id='{$app['mp_app_id']}' data-user-id='{$_SESSION['mp_user_id']}' class='btn btn-primary load_main_app' role='button'>Vezi/Voteaza</button></p>";
			$html     .= "</div>";
			$html   .= "</div>";
			$html .= "</div>";
		}
	}
	echo $html;
}

function get_app_main_view() {
	global $mpdb;
	$id = (int) $mpdb->real_escape_string($_POST['id']);
	$userid = $mpdb->real_escape_string($_POST['userid']);

	$appq = "SELECT * FROM `mp_aplications` WHERE `mp_app_id`=$id";
	$res = $mpdb->query($appq);
	$response = array();
	$html = "";
	if($res) {
		$app = $res->fetch_assoc();
		$html .="<div class='panel panel-primary single-app'>";
		$html .="<div class='panel-heading'>{$app['mp_app_name']} <span class='glyphicon glyphicon-remove remove-app pull-right'></span></div>";
		$html .=	"<div class='panel-body'>";
		$html .=		"<div class='jumbotron'>";
		$html .=			"<div class='jumbotron-photo'><img src='{$app['mp_app_picture']}' alt=''></div>";
		$html .=            "<div class='jumbotron-contents'>";
		$html .=            	"<p><span class='glyphicon glyphicon-user'></span> {$app['mp_app_author']}</p>";
		$html .=            	"{$app['mp_app_description']}";
		$html .=                app_vote_form($id, $userid);
		$html .=            "</div>";
		$html .=		"</div>";
		$html .=	"</div>";
		$html .="</div>";

		$response['type'] = 1;
		$response['html'] = $html;
	} else {
		$response['type'] = 0;
		$response['html'] = "Error";
	}

	echo json_encode($response);
	die();
}


function app_vote_form($id, $userid) {
	global $mpdb;
	$id = (int) $mpdb->real_escape_string($id);
	$userid = (int) $mpdb->real_escape_string($userid);

	$get_vote = "SELECT * FROM `mp_votes` WHERE `mp_vote_app_id`={$id} AND `mp_vote_author` = {$userid}";
	$v = array();
	$res = $mpdb->query($get_vote);
	if($res) {
		$vote = $res->fetch_assoc();
		if($res->num_rows > 0) {
			$v['exists'] = 1;
			$v['id'] = $vote['mp_vote_id'];
			$v['inov'] = $vote['mp_vote_c_inov'];
			$v['des'] = $vote['mp_vote_c_des'];
			$v['expl_res'] = $vote['mp_vote_c_expl_res'];
			$v['code_q'] = $vote['mp_vote_c_code_q'];
			$v['pres'] = $vote['mp_vote_c_pres'];

			$v['text'] = "Salveaza";
		} else {
			$v['exists'] = 0;
			$v['id'] = -1;
			$v['inov'] = 0;
			$v['des'] = 0;
			$v['expl_res'] = 0;
			$v['code_q'] = 0;
			$v['pres'] = 0;
			$v['text'] = "Voteaza";
		} 
	}

	$html = "<div class='panel panel-default app_vote_form'>
				<div class='panel-heading'>Voteaza</div>
				<div class='panel-body'>
					<form action='' method='POST' name='app_vote_form' id='app_vote_form'>
						<input type='hidden' name='app_id' value='{$id}'>
						<input type='hidden' name='vote_exists' value ='{$v['exists']}'>
						<input type='hidden' name='vote_id' value='{$v['id']}'>
						<input type='hidden' name='vote_author' value='{$userid}'/>
						<div class='row'>
							<label class='col-md-5' for=''>Inovare adusa de aplicatie</label>
							<input type='text' name='mp_vote_c_inov' class='form-control vote_field col-md-5' value='{$v['inov']}' placeholder='Inovare adusa de aplicatie'>
						</div>
						<div class='row'>
							<label class='col-md-5' for=''>Design aplicatie</label>
							<input type='text' name='mp_vote_c_des' class='form-control vote_field col-md-5' value='{$v['des']}' placeholder='Design aplicatie'>
						</div>
						<div class='row'>
							<label class='col-md-5' for=''>Exploatarea resurselor oferite de terminalul mobil </label>
							<input type='text' name='mp_vote_c_expl_res' class='form-control vote_field col-md-5' value='{$v['expl_res']}' placeholder='Exploatarea resurselor oferite de terminalul mobil '>
						</div>
						<div class='row'>
							<label class='col-md-5' for=''>Calitate cod</label>
							<input type='text' name='mp_vote_c_code_q' class='form-control vote_field col-md-5' value='{$v['code_q']}' placeholder='Calitate cod'>
						</div>
						<div class='row'>
							<label class='col-md-5' for=''>Prezentare</label>
							<input type='text' name='mp_vote_c_pres' class='form-control vote_field col-md-5' value='{$v['pres']}' placeholder='Prezentare'>
						</div>
						<div class='row'>
							<input type='submit' id='app_vote_form_button' class='btn btn-primary' name='add_vote_form_button' value='{$v['text']}'>
							<span>* Votul se poate edita oricand!</span>
						</div>
					</form>
				</div>
			</div>";
	return $html;

}

function get_app_name($id) {
	global $mpdb;

	$res = $mpdb->query("SELECT * FROM `mp_aplications` WHERE `mp_app_id`= {$id}");

	if($res){
		$res = $res->fetch_assoc();
		return $res['mp_app_name'];
	}
}

function app_vote() {
	global $mpdb;

	$vote_date = date('Y-m-d H:i:s');

	$app_id = $mpdb->real_escape_string($_POST['app_id']);
	$vote_exists = $mpdb->real_escape_string($_POST['vote_exists']);
	$vote_id = $mpdb->real_escape_string($_POST['vote_id']);
	$vote_author = $mpdb->real_escape_string($_POST['vote_author']);
	$c_inov = $mpdb->real_escape_string($_POST['c_inov']);
	$c_des = $mpdb->real_escape_string($_POST['c_des']);
	$c_expl_res = $mpdb->real_escape_string($_POST['c_expl_res']);
	$c_code_q = $mpdb->real_escape_string($_POST['c_code_q']);
	$c_pres = $mpdb->real_escape_string($_POST['c_pres']);






	if($vote_exists) {
		$vote = "UPDATE `mp_votes`
				 SET `mp_vote_c_inov`= $c_inov,
					 `mp_vote_c_des` = $c_des,
					 `mp_vote_c_expl_res` = $c_expl_res,
					 `mp_vote_c_code_q` = $c_code_q,
					 `mp_vote_c_pres` = $c_pres,
					 `mp_vote_date` = '$vote_date'
				 WHERE `mp_vote_id` = $vote_id";

	} else {
		$vote = "INSERT INTO `mp_votes`(`mp_vote_app_id`, `mp_vote_author`, `mp_vote_c_inov`, `mp_vote_c_des`, `mp_vote_c_expl_res`, `mp_vote_c_code_q`, `mp_vote_c_pres`, `mp_vote_date`)
				VALUES($app_id,$vote_author,$c_inov,$c_des,$c_expl_res,$c_code_q,$c_pres, '{$vote_date}')";
	}

	$res = $mpdb->query($vote);

	$response = array();

	if($res) {
		$response['type'] = 1;
		$response['message'] = "Datele au fost salvate!";
		$response['content'] = $res;
		$response['query'] = $vote;
	} else {
		$response['type'] = 0;
		$response['message'] = "Ceva rau s-a intamplat!";
		$response['content'] = $res;
		$response['query'] = $vote; 	
	}
	echo json_encode($response);

	die();
}

function main_list_votes() {
	global $mpdb;

	$user = $mpdb->real_escape_string($_SESSION['mp_user_id']);

	$getvotes = "SELECT * FROM `mp_votes` WHERE `mp_vote_author`={$user}";

	$res = $mpdb->query($getvotes);

	$html = "";
	$html .= "<form action='' method='POST'><table class='table table-condensed table-hover my_votes_table_full'>";
	$html .= "<thead><tr><th>#</th><th>Nume aplicatie</th><th>Inovare</th><th>Design</th><th>Exploatarea resurselor</th><th>Calitatea Codului</th><th>Prezentare</th></tr></thead>";
	$html .= "<tbody>";
	if($res) {
		while($vote = $res->fetch_assoc()) {
			$html .= "<tr data-mp-vote-id='{$vote['mp_vote_id']}'>";
			$html .= "
					<th scope='row'>{$vote['mp_vote_app_id']}</th>
					<td>". get_app_name($vote['mp_vote_app_id']) ."</td>
					<td><input type='text' class='form-control vote_field' name='mp_vote_c_inov' value='{$vote['mp_vote_c_inov']}' ></td>
					<td><input type='text' class='form-control vote_field' name='mp_vote_c_des' value='{$vote['mp_vote_c_des']}'></td>
					<td><input type='text' class='form-control vote_field' name='mp_vote_c_expl_res' value='{$vote['mp_vote_c_expl_res']}'></td>
					<td><input type='text' class='form-control vote_field' name='mp_vote_c_code_q' value='{$vote['mp_vote_c_code_q']}'></td>
					<td><input type='text' class='form-control vote_field' name='mp_vote_c_pres' value='{$vote['mp_vote_c_pres']}'></td>


					";
			$html .= "</tr>";
		}
	}

	$html .="</tbody>";
	$html .="</table>";
	$html .="</form>";
	echo $html;

}







function update_vote_single_val() {
	global $mpdb;
	$vote_id = $mpdb->real_escape_string($_POST['vote_id']);
	$cr = $mpdb->real_escape_string($_POST['cr']);
	$value = $mpdb->real_escape_string($_POST['value']);

	$update = "UPDATE `mp_votes` SET `{$cr}` = {$value} WHERE `mp_vote_id` = {$vote_id}";

	$res = $mpdb->query($update);

	$response = array();

	if($res) {
		$response['type'] = 1;
		$response['message'] = "Totul a fost salvat";
	} else {
		$response['type'] = 0;
		$response['message'] = "A aparut o eroare";
		$response['content'] = $res;
		$response['query'] = $update;
	}

	echo json_encode($response);
	die();
}













//authentication shit

function authentication() {
	session_start();
	$s = $_SESSION;

	if(
		isset($s['mp_user_id']) &&
			isset($s['mp_user_username']) &&
				isset($s['mp_user_password']) 
		) {

		global $mpdb;

		$id = $mpdb->real_escape_string($s['mp_user_id']);
		$username = $mpdb->real_escape_string($s['mp_user_username']);
		$password = $mpdb->real_escape_string($s['mp_user_password']);

		$get_user = "SELECT * FROM `mp_users` WHERE `mp_user_id` = {$id}";

		$res = $mpdb->query($get_user);
		if($res) {
			$user = $res->fetch_assoc();

			if(
				(strcmp($username, $user['mp_user_username']) == 0) &&
					(strcmp($password, $user['mp_user_password']) == 0)
				) {
				// good login. do nothing;
			} else {
				session_destroy();
				header("Location: " . HOME . "login");
			}
		}
	} else {
		session_destroy();
		header("Location: " . HOME . "login");
	}
}

function login() {
	session_start();
	session_destroy();
	if(isset($_POST['action_login'])) {
		global $mpdb;

		$username = $mpdb->real_escape_string($_POST['username']);
		$password = $mpdb->real_escape_string($_POST['password']);

		$get_user = "SELECT * FROM `mp_users` WHERE `mp_user_username`='{$username}'";

		$res = $mpdb->query($get_user);

		if($res) {
			$user = $res->fetch_assoc();
			if(
				strcmp($password, $user['mp_user_password']) == 0
				) {
				session_start();
				$_SESSION['mp_user_id'] = $user['mp_user_id'];
				$_SESSION['mp_user_username'] = $user['mp_user_username'];
				$_SESSION['mp_user_password'] = $user['mp_user_password'];
				$_SESSION['mp_user_name'] = $user['mp_user_name'];
				$_SESSION['mp_user_title'] = $user['mp_user_title'];
				$_SESSION['mp_user_type'] = $user['mp_user_type'];
				$_SESSION['mp_user_team'] = $user['mp_user_team'];
				login_attempt($username,$password,1);
				header("Location:" . HOME . "index");
				
			} else {
				login_attempt($username,$password,0);
				echo "<script>
							show_feedback(0, 'Datele introduse nu sunt corecte!')
						</script>";
			}

		} else {
			echo "<script>
					show_feedback(0, 'Datele introduse nu sunt corecte!')
				</script>";
		}

	} else if(isset($_GET['action_logout'])) {
		session_start();
		session_destroy();
		var_dump($_SESSION);
		echo "<script>
				show_feedback(1, 'You have been succesfully logged out');
				</script>";
	}
}

function login_attempt($username, $password, $type) {
	global $mpdb;

	$username = $mpdb->real_escape_string($username);
	$type = (int) $mpdb->real_escape_string($type);
	$date = date("Y-m-d H:i:s");
	$add_login = "INSERT INTO `mp_login_attempts`(`mp_login_username`,`mp_login_password`,`mp_login_date`,`mp_login_type`)
					VALUES ('{$username}','{$password}','{$date}',{$type})";
	$res = $mpdb->query($add_login);
}




function add_bootstrap_jquery() {
	?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> 
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css"> -->
	<link rel="stylesheet" href="<?php echo HOME; ?>/css/bootflat.min.css">
	
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<script>
		var ajaxurl = "<?php echo HOME; ?>/include/functions.php";
	</script>
	<?php
}













function login_check() {
	header("Locations: login.php");
}

function mp_head($path='') {
	require($path ."mp_head.php");
}

function mp_footer() {
	require("footer.php");
}













//for ajax

if(isset($_POST['action'])) {
	switch ($_POST['action']) {
		case 'add_new_setting':
			add_new_setting();
			break;
		case 'save_settings' :
			save_settings();
			break;
		case 'add_sponsor' :
			add_sponsor();
			break;
		case 'add_user' :
			add_user();
			break;
		case 'add_app' :
			add_app();
			break;
		case 'delete_app' :
			delete_app();
			break;
		case 'get_app_main_view' :
			get_app_main_view();
			break;
		case 'app_vote' : 
			app_vote();
			break;
		case 'update_vote_single_val' : 
			update_vote_single_val();
			break;
		default:
			get_settings_list();
			break;
	}
}



?>