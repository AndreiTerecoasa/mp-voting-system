<?php
	require("include/functions.php");
	authentication();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<?php mp_head('admin/'); ?>
	<link rel="stylesheet" href="<?php echo HOME; ?>css/main.css">
  
</head>
<body class="front">
	<section id="header">
		<?php include("menu.php"); ?>
	</section>
	<section class="main row no-gutter">
		<div class="panel panel-primary">
			<div class="panel-heading">Logarile mele</div>
			<div class="panel-body row">
				<?php main_list_my_logins(); ?>
			</div>
		</div>
	</section>
</body>
</html>
