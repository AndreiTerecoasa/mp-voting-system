<?php require("include/functions.php"); ?> 
<?php //authentication(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>MPVS - Login</title>
	<?php mp_head('admin/'); ?>
	<link rel="stylesheet" href="<?php echo HOME; ?>css/main.css">

</head>
<body>
	<div class="login-wrapper">
		<div class="modal">
			<div class="modal-dialog login-modal-dialog">
				<form action="" method="POST" name="login">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title">Login</h3>
						</div>
						<div class="modal-body">
								<div>
									<label for="">Username</label>
									<input type="text" name="username" placeholder="johndoe" class="form-control">
								</div>
								<div>
									<label for="">Parola</label>
									<input type="password" name="password" placeholder="johndoe" class="form-control">
								</div>

						</div>
						<div class="modal-footer">
							<input type="submit" class="btn btn-primary pull-right" name="action_login" value="Login">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<section id="footer">
		<div id="feedback"></div>
	</section>
</body>
</html>
<?php login(); ?>