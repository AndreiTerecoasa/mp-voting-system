<nav class="navbar navbar-default">
	<a href="index" class="navbar-brand"><?php get_setting('site-title'); ?></a>
	<ul class="nav navbar-nav navbar-right">
		<li><a href="about">Despre</a></li>
		<li><a href="index">Aplicatii</a></li>
		<li class="dropdown">
			 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Logat ca <?php echo $_SESSION['mp_user_name']; ?> <span class="caret"></span></a>
	          <ul class="dropdown-menu" role="menu">
	            <li><a href="myteam">Echipa mea</a></li>
	            <li><a href="myvotes">Voturile mele</a></li>
<!-- 	            <li><a href="mylogins">Logins</a></li> -->
	            <li class="divider"></li>
	            <li><a href="login?action_logout=1">Logout</a></li>
	          </ul>
		</li>
	</ul>
</nav>

